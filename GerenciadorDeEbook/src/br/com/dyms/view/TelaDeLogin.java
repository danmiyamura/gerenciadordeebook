package br.com.dyms.view;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import br.com.dyms.controller.ControllerLogin;


public class TelaDeLogin extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField usuarioLogin;
	private JPasswordField passwordField;
	
	private ControllerLogin controllerLogin;
	
	public TelaDeLogin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 540, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSistemaGerenciadorDe = new JLabel("Sistema Gerenciador de Leituras - SGL");
		lblSistemaGerenciadorDe.setFont(new Font("Trajan Pro", Font.PLAIN, 20));
		lblSistemaGerenciadorDe.setBounds(40, 0, 451, 38);
		contentPane.add(lblSistemaGerenciadorDe);
		
		JLabel label = new JLabel("Logar para continuar");
		label.setFont(new Font("Trajan Pro", Font.PLAIN, 16));
		label.setBounds(160, 92, 210, 29);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Usuário: ");
		label_1.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_1.setBounds(156, 132, 62, 29);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Senha: ");
		label_2.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_2.setBounds(156, 164, 62, 29);
		contentPane.add(label_2);
		
		usuarioLogin = new JTextField();
		usuarioLogin.setBounds(228, 134, 142, 20);
		contentPane.add("user", usuarioLogin);
		usuarioLogin.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(228, 166, 142, 20);
		contentPane.add("pass", passwordField);
		
		controllerLogin= new ControllerLogin(usuarioLogin, passwordField);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(224, 204, 112, 23);
		btnEntrar.addActionListener(controllerLogin);
		contentPane.add(btnEntrar);
		
		JButton btnCadastrar = new JButton("Cadastre-se");
		System.out.println(btnCadastrar.toString());
		btnCadastrar.addActionListener(controllerLogin);
		
		btnCadastrar.setBounds(224, 239, 112, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(224, 273, 112, 23);
		contentPane.add(btnSair);
		btnSair.addActionListener(controllerLogin);
		
		//System.out.print(contentPane.getComponents());
	}
	

	
	

}
