package br.com.dyms.view;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

public class TelaMostraNotas extends JFrame {


	private static final long serialVersionUID = -6188127078603378508L;
	private JPanel contentPane;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaMostraNotas frame = new TelaMostraNotas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public TelaMostraNotas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 520, 440);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSistemaGerenciadorDe = new JLabel("Sistema Gerenciador de Leituras - SGL");
		lblSistemaGerenciadorDe.setFont(new Font("Trajan Pro", Font.PLAIN, 20));
		lblSistemaGerenciadorDe.setBounds(40, 0, 451, 38);
		contentPane.add(lblSistemaGerenciadorDe);
		
		JLabel label_1 = new JLabel("Notas do capítulo: ");
		label_1.setFont(new Font("Trajan Pro", Font.PLAIN, 16));
		label_1.setBounds(29, 49, 378, 38);
		contentPane.add(label_1);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(407, 369, 89, 23);
		contentPane.add(btnVoltar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(29, 98, 362, 294);
		contentPane.add(scrollPane);
		
		JTextPane textPane = new JTextPane();
		scrollPane.setViewportView(textPane);
		
	}
}
