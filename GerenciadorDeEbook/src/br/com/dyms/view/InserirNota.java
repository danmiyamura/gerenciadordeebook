package br.com.dyms.view;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

public class InserirNota extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InserirNota frame = new InserirNota();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public InserirNota() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 520, 440);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSistemaGerenciadorDe = new JLabel("Sistema Gerenciador de Leituras - SGL");
		lblSistemaGerenciadorDe.setFont(new Font("Trajan Pro", Font.PLAIN, 20));
		lblSistemaGerenciadorDe.setBounds(40, 0, 451, 38);
		contentPane.add(lblSistemaGerenciadorDe);
		
		JLabel label_1 = new JLabel("Inserir notas do capítulo: ");
		label_1.setFont(new Font("Trajan Pro", Font.PLAIN, 16));
		label_1.setBounds(29, 49, 378, 38);
		contentPane.add(label_1);
		
		JButton btnVoltar = new JButton("Voltar");
		btnVoltar.setBounds(407, 369, 89, 23);
		contentPane.add(btnVoltar);
		
		JButton button = new JButton("Salvar");
		button.setBounds(407, 335, 89, 23);
		contentPane.add(button);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(29, 98, 362, 294);
		contentPane.add(scrollPane);
		
		textField = new JTextField();
		scrollPane.setViewportView(textField);
		textField.setColumns(10);
	}

}
