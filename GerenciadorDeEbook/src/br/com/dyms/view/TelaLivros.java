package br.com.dyms.view;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import br.com.dyms.controller.ControllerLogin;

public class TelaLivros extends JFrame {

	private static final long serialVersionUID = 6727926841478859747L;
	JPanel contentPane = new JPanel();
	
	
	public TelaLivros() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 540, 440);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSistemaGerenciadorDe = new JLabel("Sistema Gerenciador de Leituras - SGL");
		lblSistemaGerenciadorDe.setFont(new Font("Trajan Pro", Font.PLAIN, 20));
		lblSistemaGerenciadorDe.setBounds(40, 0, 451, 38);
		contentPane.add(lblSistemaGerenciadorDe);
		
		JLabel label_1 = new JLabel("Leituras");
		label_1.setFont(new Font("Trajan Pro", Font.PLAIN, 16));
		label_1.setBounds(86, 89, 378, 38);
		contentPane.add(label_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(47, 332, 176, -172);
		contentPane.add(scrollPane);
		
		JList list = new JList();
		scrollPane.setViewportView(list);
		
		JButton btnInserir = new JButton("Inserir");
		btnInserir.setBounds(325, 152, 116, 23);
		contentPane.add(btnInserir);
		
		JButton btnSelecionar = new JButton("Selecionar");
		btnSelecionar.setBounds(325, 190, 116, 23);
		contentPane.add(btnSelecionar);
		
		JButton btnDeletar = new JButton("Deletar");
		btnDeletar.setBounds(325, 224, 116, 23);
		contentPane.add(btnDeletar);
	}


}
