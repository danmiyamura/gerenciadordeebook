package br.com.dyms.view;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;


public class TelaNotas extends JFrame {


	private static final long serialVersionUID = -905125267189792838L;
	private JPanel contentPane;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaNotas frame = new TelaNotas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public TelaNotas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 520, 440);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSistemaGerenciadorDe = new JLabel("Sistema Gerenciador de Leituras - SGL");
		lblSistemaGerenciadorDe.setFont(new Font("Trajan Pro", Font.PLAIN, 20));
		lblSistemaGerenciadorDe.setBounds(40, 0, 451, 38);
		contentPane.add(lblSistemaGerenciadorDe);
		
		JLabel label_1 = new JLabel("Notas dos capítulos do livro: ");
		label_1.setFont(new Font("Trajan Pro", Font.PLAIN, 16));
		label_1.setBounds(10, 90, 378, 38);
		contentPane.add(label_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(47, 332, 176, -172);
		contentPane.add(scrollPane);
		
		JList<NotaLeitura> list = new JList<NotaLeitura>();
		scrollPane.setViewportView(list);
		
		JButton btnInserir = new JButton("Inserir");
		btnInserir.setBounds(325, 152, 116, 23);
		contentPane.add(btnInserir);
		
		JButton btnSelecionar = new JButton("Selecionar");
		btnSelecionar.setBounds(325, 190, 116, 23);
		contentPane.add(btnSelecionar);
		
		JButton btnDeletar = new JButton("Deletar");
		btnDeletar.setBounds(325, 224, 116, 23);
		contentPane.add(btnDeletar);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBounds(20, 345, 259, -205);
		contentPane.add(scrollPane_1);
		
		JList list_1 = new JList();
		scrollPane_1.setViewportView(list_1);
	}

}
