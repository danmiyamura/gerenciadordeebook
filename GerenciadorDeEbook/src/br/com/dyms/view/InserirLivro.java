package br.com.dyms.view;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class InserirLivro extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InserirLivro frame = new InserirLivro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public InserirLivro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 520, 440);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSistemaGerenciadorDe = new JLabel("Sistema Gerenciador de Leituras - SGL");
		lblSistemaGerenciadorDe.setFont(new Font("Trajan Pro", Font.PLAIN, 20));
		lblSistemaGerenciadorDe.setBounds(40, 0, 451, 38);
		contentPane.add(lblSistemaGerenciadorDe);
		
		JLabel label_1 = new JLabel("Inserir livro");
		label_1.setFont(new Font("Trajan Pro", Font.PLAIN, 16));
		label_1.setBounds(182, 84, 127, 38);
		contentPane.add(label_1);
		
		JLabel label = new JLabel("Nome: ");
		label.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label.setBounds(57, 156, 47, 26);
		contentPane.add(label);
		
		JLabel lblAutor = new JLabel("Autor:");
		lblAutor.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		lblAutor.setBounds(57, 193, 47, 14);
		contentPane.add(lblAutor);
		
		JLabel lblCapitulos = new JLabel("Capitulos:");
		lblCapitulos.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		lblCapitulos.setBounds(57, 228, 82, 14);
		contentPane.add(lblCapitulos);
		
		JLabel lblEditoras = new JLabel("Editora:");
		lblEditoras.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		lblEditoras.setBounds(57, 265, 80, 14);
		contentPane.add(lblEditoras);
		
		textField = new JTextField();
		textField.setBounds(108, 157, 267, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(108, 188, 267, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(132, 223, 243, 20);
		contentPane.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(132, 260, 243, 20);
		contentPane.add(textField_3);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.setBounds(78, 352, 111, 23);
		contentPane.add(btnSalvar);
		
		JButton btnSair = new JButton("Sair");
		btnSair.setBounds(234, 352, 111, 23);
		contentPane.add(btnSair);
		
		
		
		
		
	}
}
