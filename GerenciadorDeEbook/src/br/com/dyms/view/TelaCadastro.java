package br.com.dyms.view;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class TelaCadastro extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8288277680461206490L;
	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JTextField textField_1;

	/**
	 * Create the frame.
	 */
	public TelaCadastro() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 540, 420);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("Sistema Gerenciador de Leituras - SGL");
		label.setFont(new Font("Trajan Pro", Font.PLAIN, 20));
		label.setBounds(40, 0, 451, 38);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("Preencha os dados para se cadastrar");
		label_1.setFont(new Font("Trajan Pro", Font.PLAIN, 16));
		label_1.setBounds(75, 60, 378, 38);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("Usuário: ");
		label_2.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_2.setBounds(53, 109, 62, 29);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("Nome: ");
		label_3.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_3.setBounds(53, 149, 62, 29);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("E-mail: ");
		label_4.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_4.setBounds(53, 189, 62, 29);
		contentPane.add(label_4);
		
		JLabel label_5 = new JLabel("CPF: ");
		label_5.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_5.setBounds(53, 229, 62, 29);
		contentPane.add(label_5);
		
		JLabel label_6 = new JLabel("Repetir senha: ");
		label_6.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_6.setBounds(53, 269, 62, 29);
		contentPane.add(label_6);
		label_6.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_6.setBounds(10, 309, 102, 29);
		
		textField = new JTextField();
		textField.setBounds(122, 149, 341, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(122, 189, 341, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField = new JTextField();
		textField.setBounds(122, 229, 341, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(122, 269, 341, 20);
		contentPane.add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(122, 309, 341, 20);
		contentPane.add(passwordField_1);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(219, 349, 115, 23);
		contentPane.add(btnCadastrar);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(122, 111, 341, 20);
		contentPane.add(textField_1);
		
		JLabel label_8 = new JLabel("Senha: ");
		label_8.setFont(new Font("Trajan Pro", Font.PLAIN, 12));
		label_8.setBounds(53, 269, 102, 29);
		contentPane.add(label_8);	

	}

}
