package br.com.dyms.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPasswordField;
import javax.swing.JTextField;

import br.com.dyms.dao.PessoaDao;
import br.com.dyms.model.Usuario;
import br.com.dyms.view.TelaCadastro;
import br.com.dyms.view.TelaLivros;

public class ControllerLogin implements ActionListener {

	private PessoaDao pessoaDao;
	private JTextField usuarioLogin;
	private JPasswordField passwordField;
	
	
	public ControllerLogin(JTextField usuarioLogin, JPasswordField passwordField) {
		this.usuarioLogin = usuarioLogin;
		this.passwordField = passwordField;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("Cadastre-se".equalsIgnoreCase(cmd)) {
			
			try {
				TelaCadastro frame = new TelaCadastro();
				
				frame.setVisible(true);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			
		} else if ("Entrar".equalsIgnoreCase(cmd)) {
			
			
			Usuario u = generatedLoginFromView();
			pessoaDao = new PessoaDao();
					
			boolean correctPass	= pessoaDao.findOne(u.getUsername(), new String(u.getPassword()).toString());
			
			if (correctPass == true) {
				try {
					TelaLivros frame = new TelaLivros();
					frame.setVisible(true);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			
		} else if ("Sair".equalsIgnoreCase(cmd)) {
			
			System.exit(0);
		}
		
	}
	
	public Usuario generatedLoginFromView () {
		try {
			Usuario u = new Usuario(usuarioLogin.getText(), passwordField.getPassword());
			return u;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}

