package br.com.dyms;

import java.awt.EventQueue;

import javax.swing.JFrame;

import br.com.dyms.view.TelaDeLogin;

public class DymsMain {
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run () {
				try {
					TelaDeLogin frame = new TelaDeLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
