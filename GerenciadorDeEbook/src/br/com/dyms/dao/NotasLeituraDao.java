package br.com.dyms.dao;

import java.util.List;

import br.com.dyms.model.NotaLeitura;

public class NotasLeituraDao {
	
	public void save(NotaLeitura notaLeitura) {
		manager.persist(notaLeitura);
	}
	
	public List<NotaLeitura> findAll(){
		return manager.createQuery("select s from NotasLeitura s", NotaLeitura.class).getResultList();
	}
	
	public NotaLeitura findOne(NotaLeitura notaId) {
		return manager.find(NotaLeitura.class, notaId);
	}
	
	public void delete(NotaLeitura notaId) {
		manager.remove(findOne(notaId));
	}
}
