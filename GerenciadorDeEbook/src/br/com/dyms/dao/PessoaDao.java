package br.com.dyms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.dyms.jdbc.ConnectionFactory;
import br.com.dyms.model.Pessoa;
import br.com.dyms.model.Usuario;

public class PessoaDao {
	
	private Connection con;
	
	public PessoaDao() {
		try {
			new ConnectionFactory();
			con = ConnectionFactory.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void save(Pessoa pessoa) {
		String sql = "insert into pessoas (usuario, nome, email, cpf) values (?, ?, ?, ?);";
		
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, pessoa.getUsuario());
			stmt.setString(2, pessoa.getNome());
			stmt.setString(3, pessoa.getEmail());
			stmt.setString(4, pessoa.getCpf());
			
			stmt.execute();			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public boolean findOne(String usuario, String password) {

		String sql = "select * from usuario where username = ? and senha = ?;";
		
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement(sql);
			System.out.println(usuario);
			System.out.println(password);
			stmt.setString(1, usuario);
			stmt.setString(2, password);
			
			ResultSet rs = stmt.executeQuery();
			
			if(rs != null) {
				return true;
			}
			
			con.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false; 
	}
}
