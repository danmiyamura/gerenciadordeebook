package br.com.dyms.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.dyms.jdbc.ConnectionFactory;
import br.com.dyms.model.Livro;


public class LivroDao {

	private Connection con;
	
	public LivroDao() {
		try {
			new ConnectionFactory();
			con = ConnectionFactory.getConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void save(Livro livro) {
		String sql = "insert into livros (titulo, autor, editora, capitulos) values (?, ?, ?, ?);";
		
		PreparedStatement stmt;
		try {
			stmt = con.prepareStatement(sql);
			stmt.setString(1, livro.getTitulo());
			stmt.setString(2, livro.getAutor());
			stmt.setString(3, livro.getEditora());
			stmt.setInt(4, livro.getCapitulos());
			
			stmt.execute();
			stmt.getResultSet();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public List<Livro> findAll() {
		List<Livro> livros = new ArrayList<Livro>();
		
		String sql = "SELECT * FROM LIVROS";
		
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Livro livro = new Livro(rs.getString("titulo"), rs.getString("autor"), rs.getString("editora"), rs.getInt("capitulos"));
				livro.setId(rs.getInt("id"));

				livros.add(livro);
			}
			
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return livros;
	}
	
	public void delete(Integer livroId) {
		
		String sql = "DELETE FROM LIVROS WHERE ID = ?";
		
		PreparedStatement stmt;
		
		try {
			stmt = con.prepareStatement(sql);
			stmt.setInt(1, livroId);
			stmt.execute();
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
