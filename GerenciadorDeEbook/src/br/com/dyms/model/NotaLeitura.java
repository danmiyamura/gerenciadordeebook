package br.com.dyms.model;

public class NotaLeitura {
	
	private Integer id;
	private Livro livro;
	private String anotacao;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAnotacao() {
		return anotacao;
	}
	public void setAnotacao(String anotacao) {
		this.anotacao = anotacao;
	}
	public Integer getLivro() {
		return livro.getId();
	}
	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	
	
}
