package br.com.dyms.model;

import java.util.HashSet;
import java.util.Set;

import br.com.dyms.exception.CadastroInvalidoException;

public class Usuario {

	private Integer id;
	private String username;
	private char[] password;
	
	public Usuario(String username, char[] cs) throws Exception{
		
		if((!username.isEmpty()) ||
				(username.length() >= 8)) {
			this.username = username;
			this.password = cs;
		} else {
			throw new CadastroInvalidoException();
		}

	}
	
	public Usuario() {
		// TODO Auto-generated constructor stub
	}
	
	private Set<Livro> livros = new HashSet<>();

	public Set<Livro> getLivros() {
		return livros;
	}

	public void setLivros(Set<Livro> livros) {
		this.livros = livros;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}
}
