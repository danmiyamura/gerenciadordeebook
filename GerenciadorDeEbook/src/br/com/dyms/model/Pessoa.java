package br.com.dyms.model;

public class Pessoa {

	private Integer id;
	private Usuario usuario;
	private String nome;
	private String email;
	private String cpf;
	
	public Pessoa(Usuario usuario, String nome, String email, String cpf) {
		this.usuario = usuario;
		this.nome = nome;
		this.email = email;
		this.cpf = cpf;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getUsuario() {
		return usuario.getId();
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
