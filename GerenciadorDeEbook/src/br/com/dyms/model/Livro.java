package br.com.dyms.model;

import java.util.HashSet;
import java.util.Set;


public class Livro {

	private Integer id;
	private String titulo;
	private String autor;
	private String editora;
	private Integer capitulos;
	private Set<NotaLeitura> notas = new HashSet<>();
	
	public Livro(String titulo, String autor, String editora, Integer capitulos) {
		this.titulo = titulo;
		this.autor = autor;
		this.editora = editora;
		this.capitulos = capitulos;
	}

	public Set<NotaLeitura> getNotas() {
		return notas;
	}

	public void setNotas(Set<NotaLeitura> notas) {
		this.notas = notas;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEditora() {
		return editora;
	}

	public void setEditora(String editora) {
		this.editora = editora;
	}

	public Integer getCapitulos() {
		return capitulos;
	}

	public void setCapitulos(Integer capitulos) {
		this.capitulos = capitulos;
	}

}
